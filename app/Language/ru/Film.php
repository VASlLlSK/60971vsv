<?php
/**
 * Name:  Ion Auth Lang - Russian (UTF-8)
 *
 * Author: Ben Edmunds
 * 		  ben.edmunds@gmail.com
 *         @benedmunds
 * Translation:  Petrosyan R.
 *             for@petrosyan.rv.ua
 *
 * Location: http://github.com/benedmunds/ion_auth/
 *
 * Created:  03.26.2010
 *
 * Description:  Russian language file for Ion Auth messages and errors
 *
 */

return [
    // Account Creation
    'film_create_success' 	  	 => 'Фильм успешно создан',
    'rating_update_success'      => 'Фильм успешно изменён',
];