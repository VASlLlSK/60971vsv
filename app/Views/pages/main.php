<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container-fluid" style="padding: 0!important;">
    <div class="m-3 shadow rounded" style="position: relative!important; padding: 0!important;
     background-image: url(https://avatars.mds.yandex.net/get-altay/1545421/2a0000016ac5d21fb8c60ad46a45f07b6f46/XXXL)">
        <div class="rounded" style="padding: 0!important; background-color: #222222aa!important; position: relative!important;">
            <div class="jumbotron shadow rounded bg-transparent">
                <div class="container-fluid">
                    <h1 class="display-3" style="color: #fafafa">Синема парк!</h1>
                    <p style="color: #fafafa">Синема Парк — шестизальный кинотеатр на 300 мест,
                        расположенный в ТРЦ «Сургут Сити Молл». Мультиплекс сочетает в
                        себе все самые актуальные мировые тенденции кинопоказа, включая суперзалы
                        IMAX на 56 мест и уникальный зал 4DX, в котором зрители могут испытать
                        такие спецэффекты, как ветер, туман, вспышка молнии и ароматы.
                        Для наиболее взыскательных зрителей предусмотрены залы повышенной
                        комфортности — Relax и Jolly. </p>
                    <p>
                        <?php if (! $ionAuth->loggedIn()): ?>
                        <a class="btn btn-primary btn-lg mr-3" href="auth/login" role="button">Войти</a>
                        <a class="btn btn-primary btn-lg" href="auth/register_user" role="button">Регистрация</a>
                        <?php else: ?>
                        <a class="btn btn-primary btn-lg mr-3" href="auth/logout" role="button">Выйти</a>
                        <?php endif ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <h2>Heading</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            <p><a class="btn btn-secondary" href="#" role="button">View details »</a></p>
        </div>
        <div class="col-md-4">
            <h2>Heading</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            <p><a class="btn btn-secondary" href="#" role="button">View details »</a></p>
        </div>
        <div class="col-md-4">
            <h2>Heading</h2>
            <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
            <p><a class="btn btn-secondary" href="#" role="button">View details »</a></p>
        </div>
    </div>
    <hr>
</div>

<?= $this->endSection() ?>
