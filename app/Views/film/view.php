<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php if (!empty($film)) : ?>
            <div class="card mb-3 rounded" style="max-width: 900px">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-start">
                        <img src="<?= esc($film['picture_url']); ?>"
                             class="img-fluid rounded-left" alt="<?= esc($film['name']); ?>">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= esc($film['name']); ?></h5>

                            <div class="d-flex justify-content-start">
                                <div class="my-0">Хронометраж фильма: </div>
                                <p class="card-text ml-2"><?= esc($film['length']); ?> мин</p>
                            </div>
                            <div class="mt-4">Все сеансы:</div>
                            <div class="container">
                                <div class="d-flex justify-content-between">
                                    <div class="my-0 mt-2">Зал:</div>
                                    <?php if (!empty($halls)) : ?>

                                        <?php foreach ($halls as $item): ?>
                                            <span class="badge badge-info m-1 align-self-end"><?= esc($item['name']); ?></span>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <p>Нет свободных залов.</p>
                                    <?php endif ?>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <div class="my-0 mt-2">Дата:</div>
                                    <?php if (!empty($sessions)) : ?>
                                        <?php foreach ($sessions as $item): ?>
                                            <span class="badge badge-info m-1 align-self-end btn"><?= esc($item['time']); ?></span>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <p>Нет свободной даты.</p>
                                    <?php endif ?>
                                </div>
                                <hr>
                            </div>
                            <a class="btn btn-primary ml-3" href="<?= base_url()?>/film/edit/<?= $film['id'] ?>">Изменить</a>
                            <a class="btn btn-danger m-1" href="<?= base_url()?>/film/delete/<?= $film['id'] ?>">Удалить</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <p>Фильм не найден.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>