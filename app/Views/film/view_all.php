<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container">
        <h2>Все фильмы</h2>

        <?php if (!empty($film) && is_array($film)) : ?>

            <?php foreach ($film as $item): ?>

                <div class="card mb-3 rounded" style="max-width: 540px;">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center">
                                <img class="img-fluid rounded-left" src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['name']); ?>">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title"><?= esc($item['name']); ?></h5>
                                <p class="card-text">Хронометраж фильма: <?= esc($item['length']); ?> мин</p>
                                <a href="<?= base_url()?>/index.php/film/view/<?= esc($item['id']); ?>" class="btn btn-primary mt-1">Выбрать сеанс</a>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти фильмы.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>