<?php namespace App\Models;
use CodeIgniter\Model;

class SessionModel extends Model
{
    protected $table = 'session'; //таблица, связанная с моделью
    public function getSessions($idFilm)
    {
        return $this->where(['id_film' => $idFilm])->findAll();
    }
}