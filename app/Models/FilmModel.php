<?php namespace App\Models;
use CodeIgniter\Model;

class FilmModel extends Model
{
    protected $table = 'film'; //таблица, связанная с моделью
    protected $allowedFields = ['name', 'length', 'picture_url'];
    public function getFilm($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}