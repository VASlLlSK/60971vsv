<?php namespace App\Models;
use CodeIgniter\Model;

class HallModel extends Model
{
    protected $table = 'hall'; //таблица, связанная с моделью
    public function getHall($id)
    {
        return $this->where(['id' => $id])->first();
    }
}