<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;
use function Webmozart\Assert\Tests\StaticAnalysis\string;

class Cinema extends Migration
{
	public function up()
	{
        // activity_type
        if (!$this->db->tableexists('film'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);
            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'length' => array('type' => 'INT', 'null' => FALSE),
                'img' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
            ));
            // create table
            $this->forge->createtable('film', TRUE);
        }

        if (!$this->db->tableexists('hall'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            // create table
            $this->forge->createtable('hall', TRUE);
        }

        if (!$this->db->tableexists('place'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_hall' => array('type' => 'INT', 'unsigned' => TRUE),
                'row' => array('type' => 'INT', 'null' => FALSE),
                'number' => array('type' => 'INT', 'null' => FALSE),
                'price_bracket' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('id_hall','hall','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('place', TRUE);
        }

        if (!$this->db->tableexists('session'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_hall' => array('type' => 'INT', 'unsigned' => TRUE),
                'id_film' => array('type' => 'INT', 'unsigned' => TRUE),
                'time' => array('type' => 'DATETIME', 'null' => TRUE),
            ));
            $this->forge->addForeignKey('id_hall','hall','id','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('id_film','film','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('session', TRUE);
        }

        if (!$this->db->tableexists('price'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_session' => array('type' => 'INT', 'unsigned' => TRUE),
                'price_bracket' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'price' => array('type' => 'INT', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('id_session','session','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('price', TRUE);
        }

        if (!$this->db->tableexists('ticket'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_session' => array('type' => 'INT', 'unsigned' => TRUE),
                'id_place' => array('type' => 'INT', 'unsigned' => TRUE),
                'client' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('id_session','session','id','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('id_place','place','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('ticket', TRUE);
        }
	}

	//--------------------------------------------------------------------

	public function down()
	{
        $this->forge->droptable('film');
        $this->forge->droptable('hall');
        $this->forge->droptable('place');
        $this->forge->droptable('session');
        $this->forge->droptable('price');
        $this->forge->droptable('ticket');
	}
}
