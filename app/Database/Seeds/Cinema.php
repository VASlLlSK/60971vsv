<?php namespace App\Database\Seeds;

use Carbon\Carbon;
use CodeIgniter\Database\Seeder;

class Cinema extends Seeder
{
    public function run()
    {
        $data = [
            'name' => 'Гудбай, Америка!',
            'length' => 108,
            'picture_url' => 'https://im0-tub-ru.yandex.net/i?id=f8ae30419b9ebe254b13ae6e7ccf4ce5&n=13'
        ];
        $this->db->table('film')->insert($data);
        $data = [
            'name' => 'Китобой',
            'length' => 100,
            'picture_url' => 'http://media.filmz.ru/photos/full/filmz.ru_f_261895.jpg'
        ];
        $this->db->table('film')->insert($data);
        $data = [
            'name' => 'Довод',
            'length' => 153,
            'picture_url' => 'https://u.kanobu.ru/editor/images/52/5ee8c86f-ed76-4379-9b99-57b7b62f2330.jpg'
        ];
        $this->db->table('film')->insert($data);
        $data = [
            'name' => 'Семейка Бигфутов',
            'length' => 91,
            'picture_url' => 'https://im0-tub-ru.yandex.net/i?id=6a109ef3c311f78e3257070f88e97c0b&n=13'
        ];
        $this->db->table('film')->insert($data);
        $data = [
            'name' => 'Мулан',
            'length' => 115,
            'picture_url' => 'https://www.soyuz.ru/public/uploads/files/3/7397435/20191205175618b23ab5f1d5.jpg'
        ];
        $this->db->table('film')->insert($data);
        $data = [
            'name' => 'Капоне. Лицо со шрамом',
            'length' => 109,
            'picture_url' => 'https://fiveyellowstars.ru/upload/iblock/30d/30d0cb7b67b94108b3fdf958ad8a7979.jpg'
        ];
        $this->db->table('film')->insert($data);
        $data = [
            'name' => 'Честный вор',
            'length' => 104,
            'picture_url' => 'http://go-data-vyhoda.com/uploads/base/250x375/0b24a64f9aae710982ff00804f4f7193_local.jpg'
        ];
        $this->db->table('film')->insert($data);
        $data = [
            'name' => 'Гномы в деле!',
            'length' => 83,
            'picture_url' => 'https://www.startfilm.ru/images/base/film/f_670748/big_startfilmru1439737.jpg'
        ];
        $this->db->table('film')->insert($data);

        for($i = 1; $i <=6; $i++){
            $data = [
                'name' => $i.' зал'
            ];
            $this->db->table('hall')->insert($data);
        }

        for($i = 1; $i <=6; $i++){
            for($j = 1; $j <=8; $j++) {
                for ($k = 1; $k <= 7; $k++) {
                    $data = [
                        'id_hall' => $i,
                        'row' => $j,
                        'number' => $k,
                        'price_bracket' => 'Бюджетный'
                    ];
                    $this->db->table('place')->insert($data);
                }
            }
        }

        $data = [
            'id_hall' => 6,
            'id_film' => 4,
            'time' => Carbon::createFromFormat('Y-m-d H', '2020-10-12 0')
        ];
        $this->db->table('session')->insert($data);
        $data = [
            'id_hall' => 3,
            'id_film' => 6,
            'time' => Carbon::createFromFormat('Y-m-d H', '2020-10-12 16')
        ];
        $this->db->table('session')->insert($data);
        $data = [
            'id_hall' => 5,
            'id_film' => 7,
            'time' => Carbon::createFromFormat('Y-m-d H', '2020-10-12 16')
        ];
        $this->db->table('session')->insert($data);
        $data = [
            'id_hall' => 1,
            'id_film' => 8,
            'time' => Carbon::createFromFormat('Y-m-d H', '2020-10-12 22')
        ];
        $this->db->table('session')->insert($data);
        $data = [
            'id_hall' => 3,
            'id_film' => 6,
            'time' => Carbon::createFromFormat('Y-m-d H', '2020-10-12 19')
        ];
        $this->db->table('session')->insert($data);
        $data = [
            'id_hall' => 5,
            'id_film' => 3,
            'time' => Carbon::createFromFormat('Y-m-d H', '2020-10-12 16')
        ];
        $this->db->table('session')->insert($data);
        $data = [
            'id_hall' => 5,
            'id_film' => 7,
            'time' => Carbon::createFromFormat('Y-m-d H', '2020-10-13 22')
        ];
        $this->db->table('session')->insert($data);
        $data = [
            'id_hall' => 1,
            'id_film' => 5,
            'time' => Carbon::createFromFormat('Y-m-d H', '2020-10-13 12')
        ];
        $this->db->table('session')->insert($data);
        $data = [
            'id_hall' => 3,
            'id_film' => 3,
            'time' => Carbon::createFromFormat('Y-m-d H', '2020-10-13 19')
        ];
        $this->db->table('session')->insert($data);
        $data = [
            'id_hall' => 5,
            'id_film' => 9,
            'time' => Carbon::createFromFormat('Y-m-d H', '2020-10-13 14')
        ];
        $this->db->table('session')->insert($data);
        $data = [
            'id_hall' => 4,
            'id_film' => 8,
            'time' => Carbon::createFromFormat('Y-m-d H', '2020-10-13 22')
        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_session' => 2,
            'price_bracket' => 'Премиум',
            'price' => 600
        ];
        $this->db->table('price')->insert($data);
        $data = [
            'id_session' => 1,
            'price_bracket' => 'Классический',
            'price' => 250
        ];
        $this->db->table('price')->insert($data);
        $data = [
            'id_session' => 1,
            'price_bracket' => 'Классический',
            'price' => 250
        ];
        $this->db->table('price')->insert($data);
        $data = [
            'id_session' => 3,
            'price_bracket' => 'Бюджетный',
            'price' => 170
        ];
        $this->db->table('price')->insert($data);
        $data = [
            'id_session' => 6,
            'price_bracket' => 'Классический',
            'price' => 250
        ];
        $this->db->table('price')->insert($data);
        $data = [
            'id_session' => 4,
            'price_bracket' => 'Классический',
            'price' => 250
        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 3,
            'id_place' => 253,
            'client' => 'Василенко С.В.'
        ];
        $this->db->table('ticket')->insert($data);
        $data = [
            'id_session' => 4,
            'id_place' => 232,
            'client' => 'Иванов И.И.'
        ];
        $this->db->table('ticket')->insert($data);
        $data = [
            'id_session' => 1,
            'id_place' => 243,
            'client' => 'Панков Д.В.'
        ];
        $this->db->table('ticket')->insert($data);
        $data = [
            'id_session' => 4,
            'id_place' => 146,
            'client' => 'Давыдов А.А.'
        ];
        $this->db->table('ticket')->insert($data);
        $data = [
            'id_session' => 6,
            'id_place' => 125,
            'client' => 'Данилова Г.А.'
        ];
        $this->db->table('ticket')->insert($data);
        $data = [
            'id_session' => 2,
            'id_place' => 129,
            'client' => 'Курова Д.А.'
        ];
        $this->db->table('ticket')->insert($data);
        $data = [
            'id_session' => 5,
            'id_place' => 156,
            'client' => 'Хомякова Е.Н.'
        ];
        $this->db->table('ticket')->insert($data);
        $data = [
            'id_session' => 6,
            'id_place' => 106,
            'client' => 'Горин Г.Г.'
        ];
        $this->db->table('ticket')->insert($data);
        $data = [
            'id_session' => 4,
            'id_place' => 6,
            'client' => 'Игнатьев Р.П.'
        ];
        $this->db->table('ticket')->insert($data);
        $data = [
            'id_session' => 8,
            'id_place' => 97,
            'client' => 'Скрипинский Б.А.'
        ];
        $this->db->table('ticket')->insert($data);
        $data = [
            'id_session' => 9,
            'id_place' => 73,
            'client' => 'Зубенко М.П.'
        ];
        $this->db->table('ticket')->insert($data);
        $data = [
            'id_session' => 11,
            'id_place' => 80,
            'client' => 'Терентьев М.П.'
        ];
        $this->db->table('ticket')->insert($data);
        $data = [
            'id_session' => 9,
            'id_place' => 274,
            'client' => 'Храмов Д.Д.'
        ];
        $this->db->table('ticket')->insert($data);
    }
}