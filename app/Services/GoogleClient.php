<?php namespace App\Services;

use Google_Client;

class GoogleClient
{
    private $google_client;
    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('105267819056-eeius8ctrfnlbk3aqltaln6gn0kskipf.apps.googleusercontent.com');
        $this->google_client->setClientSecret('A_bY562v-96Dyyc1-4x_BEJR');
        $this->google_client->setRedirectUri(base_url().'/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }
    public function getGoogleClient()
    {
        return $this->google_client;
    }

}