<?php namespace App\Controllers;

use App\Models\FilmModel;
use App\Models\HallModel;
use App\Models\SessionModel;
use Aws\S3\S3Client;
use CodeIgniter\Controller;
use mysql_xdevapi\Session;

class Film extends BaseController
{

    public function index() //Обображение всех записей
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new FilmModel();
        $data ['film'] = $model->getFilm();
        echo view('film/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new FilmModel();
        $sessions = new SessionModel();
        $halls = new HallModel();

        $data ['film'] = $model->getFilm($id);
        $dataa = $sessions->getSessions($id);
        $data ['sessions'] = $dataa;
        $dd = null;
        foreach ($dataa as $item){
            $dd[] = $halls->getHall($item['id_hall']);
        }
        $data ['halls'] = $dd;
        echo view('film/view', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'name' => 'required|min_length[3]|max_length[255]',
                'length'  => 'required',
                'picture'  => 'uploaded[picture]|is_image[picture]|max_size[picture,1024]',
            ]))
        {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }


            $model = new FilmModel();
            $data = [
                'name' => $this->request->getPost('name'),
                'length' => $this->request->getPost('length'),
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);

            session()->setFlashdata('message', lang('Film.film_create_success'));
            return redirect()->to('/film');
        }
        else
        {
            return redirect()->to('/film/create')->withInput();
        }
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('film/create', $this->withIon($data));
    }

    public function update()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'id' => 'required',
                'name' => 'required|min_length[3]|max_length[255]',
                'length'  => 'required',
                'picture'  => 'uploaded[picture]|is_image[picture]|max_size[picture,1024]',
            ]))
        {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }

//            $model = new FilmModel();
//            $data = [
//                'name' => $this->request->getPost('name'),
//                'length' => $this->request->getPost('length'),
//            ];
//            if (!is_null($insert))
//                $data['picture_url'] = $insert['ObjectURL'];
//            $model->save($data);


            $model = new FilmModel();
            $model->save([
                'id' => $this->request->getPost('id'),
                'name' => $this->request->getPost('name'),
                'length' => $this->request->getPost('length'),
                'picture_url' => $insert['ObjectURL'],
            ]);
            session()->setFlashdata('message', lang('Film.rating_update_success'));
            return redirect()->to('/film');
        }
        else
        {
            return redirect()->to('/film/edit/'.$this->request->getPost('id'))->withInput();
        }
    }


    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new FilmModel();

        helper(['form']);
        $data ['film'] = $model->getFilm($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('film/edit', $this->withIon($data));

    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new FilmModel();
        $model->delete($id);
        return redirect()->to('/film');
    }

}